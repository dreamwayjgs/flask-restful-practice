def deco(fn):
    def wrapper(*args, **kwargs):
        name = args[0]
        if name == "한국인":
            print("안녕!")
        print("함수전")
        print(fn(*args, **kwargs))
        print("함수후")

    return wrapper


class Deco2:
    def __init__(self, fn):
        self.fn = fn

    def __call__(self, *args, **kwargs):
        name = args[0]
        if name == "한국인":
            print("안녕!")
        print("함수전")
        print(self.fn(*args, **kwargs))
        print("함수후")


@deco
def say_hello(name):
    return "hello " + name


@Deco2
def say_hello2(name):
    return "hell " + name


say_hello("jgs")
say_hello("한국인")
say_hello2("한국인")
