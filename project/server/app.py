from flask import Flask, request
from flask.json import jsonify
from flask_login import LoginManager, login_user, current_user
import json
from flask_login.mixins import UserMixin
from flask_login.utils import login_required
from dataclasses import dataclass


app = Flask(__name__)
app.config["SECRET_KEY"] = "dkeocndgkwkwha"


@dataclass
class User(UserMixin):
    id: int
    name: str


Users = [User(1, "JGS"), User(2, "SAR")]

login_manager = LoginManager()
login_manager.init_app(app)


@login_manager.user_loader
def load_user(user_id):
    print("Trying to user Load", user_id)
    for user in Users:
        if user.id == int(user_id):
            return user


@app.route("/")
def index():
    return "hello\n"


@app.route("/data")
def send_data():
    data = {"data": "blahblah"}
    return json.dumps(data)


@app.route("/secret")
@login_required
def secret_data():
    print("SECRET PLACE")
    return "SECRET ENDPOINT"


@app.route("/signup", methods=["POST"])
def signup():
    headers = request.headers.get("Content-Type")
    print("헤더", headers)
    id = int(request.form.get("id", 0))
    for user in Users:
        if user.id == id:
            print("User LOGIN", user)
            login_user(user, remember=True)
            print(current_user)
            return "Success"
    return "Failed"


if __name__ == "__main__":
    app.run(debug=True, host="0.0.0.0")
