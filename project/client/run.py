import requests


def tprint(x):
    print(type(x), x)


with requests.Session() as session:
    url = "http://localhost:5000/"
    headers = {"Content-Type": "application/x-www-form-urlencoded"}
    data = {"id": 1}
    r = session.post(url + "signup", data=data, headers=headers)
    print(r.content)

    r = session.get(url + "secret")
    print(r.content)
